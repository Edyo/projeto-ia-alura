import { ChatSession, GoogleGenerativeAI, HarmBlockThreshold, HarmCategory, type GenerationConfig, type SafetySetting } from "@google/generative-ai";

const config = useRuntimeConfig()
const genAI = new GoogleGenerativeAI(config.public.API_KEY);
const generationConfig: GenerationConfig = {
    candidateCount: 1,
    temperature: 1,
}
const safetySettings: SafetySetting[] = [
    {
        category: HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT,
        threshold: HarmBlockThreshold.BLOCK_NONE
    },
    {
        category: HarmCategory.HARM_CATEGORY_HARASSMENT,
        threshold: HarmBlockThreshold.BLOCK_NONE
    },
    {
        category: HarmCategory.HARM_CATEGORY_HATE_SPEECH,
        threshold: HarmBlockThreshold.BLOCK_NONE
    },
    {
        category: HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT,
        threshold: HarmBlockThreshold.BLOCK_NONE
    },
]

const model = genAI.getGenerativeModel({ 
    model: "gemini-1.5-pro-latest",
    generationConfig: generationConfig,
    safetySettings: safetySettings,
    systemInstruction: 'Você é um cinéfilo. Além disso, sempre que você daria uma quebra de linha, você escreve \n',
});

let chat: ChatSession

export async function generate(prompt: string) {
    const result = await chat.sendMessageStream(prompt);
    return result
}

export async function findMovies(input1: string, input2: string) {
    chat = model.startChat({
        history: []
    })
    const msg = `-(quero que sua resposta seja de 10 filmes já lançados separados por ||. Se não for possível encontrar filme que agrade as duas pessoas, responder com um filme mais proximo de agradar os dois, e se isso não for possível, que agrade pelo menos um dos dois. Não quero justificativa nem nada alem do nome do filme)

    Usuário: [
    Pessoa 1: Estou com vontade de assistir algum filme do Nicholas Cage
    Pessoa 2: Queria assistir um filme de Drama policial, ou algo parecido
    ]
    
    Gemini: O Senhor das Armas || Fúria Incontrolável || 8MM || A Outra Face || Con Air - A Rota da Fuga || O Rochedo || Adaptação || O Sacrifício || Mandy - Sede de Vingança || Cor Selvagem
    
    ------------------------------------------------------------------------------
    
    Usuário: [
    Pessoa 1: Estou com vontade de assistir algum filme de Terror psicológico
    Pessoa 2: Queria assistir um filme de comédia, nada muito pesado.
    ]
    
    Gemini: Psicose || Garota Exemplar || O Iluminado || Corra!, O Babadook || A Visita || A Bruxa || O Farol || Midsommar - O Mal Não Espera a Noite || A Cabana na Floresta
    
    ----------------------------------------------------------------------
    
    Usuário: [
        Pessoa 1: ${input1}
        Pessoa 2: ${input2}
    ]

    Gemini:`

    const result = await chat.sendMessage(msg)

    return result.response.text()
}