## Chega de brigas pelo controle remoto!

Cansado de intermináveis debates sobre qual filme assistir? Apresentamos o **[Chega de brigas pelo controle remoto!](https://projeto-ia-alura.vercel.app/)**, uma solução que utiliza o poder do Gemini para encontrar o filme perfeito para duas pessoas, mesmo com gostos distintos!

**Como funciona?**

[Chega de brigas pelo controle remoto!](https://projeto-ia-alura.vercel.app/) combina a inteligência do Gemini, um modelo de linguagem avançado, com a flexibilidade do Nuxt.js e Vue.js para criar um sistema de recomendação de filmes.

1. **Digite que tipo de filme cada pessoa quer assistir:** Nosso sistema usa essa informação para entender as preferências individuais.
2. **Gemini entra em ação:**  Ele analisa as escolhas, identifica padrões e gera uma lista de filmes que ambos provavelmente apreciarão.
3. **Chega de brigas, aproveite o filme!** Escolha o filme da lista que mais te agrada e prepare a pipoca!
E se ainda houverem dúvidas, sintam-se livres para conversar com o Gemini sobre as recomendações recebidas.

**Tecnologia:**

* Gemini API
* Nuxt.js
* Vue.js

**Divirta-se explorando novos filmes juntos!**