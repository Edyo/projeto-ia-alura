// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  runtimeConfig: {
    public: {
      API_KEY: process.env.API_KEY,
      IMDB_KEY: process.env.IMDB_KEY,
    }
  },
  components: [
    {
      path: '~/components', // will get any components nested in let's say /components/test too
      pathPrefix: false, // <------------------- here
      extensions: ['vue'],
    },
  ],
  experimental: {
    asyncContext: true
  }
})
